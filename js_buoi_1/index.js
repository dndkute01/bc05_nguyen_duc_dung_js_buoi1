/**
 * Bai 1
 *
 * Input: Lương 1 ngày: 100.000, số ngày làm 20
 *
 * Step:
 * +step1: Tạo một biến số ngày làm = 20 ngày và biến lương một ngày.
 * +step2: Tạo biến chứa kết quả cần tìm.
 * +step 3: Áp dụng công thức tổng lương bằng lương 1 ngày * số ngày làm .
 *
 * Output: 2.000.000
 */

var so_ngay_lam = 20;
var luong_1_ngay = 100000;
var tong_luong;

tong_luong = so_ngay_lam * luong_1_ngay;
console.log("Tong luong =", tong_luong);

/**
 * Input: 5 số thực 1,2,3,4,5
 *
 * step:
 * +step1: Tạo năm biến để nhập giá trị của 5 số thực
 * +step2: Tạo biến giá trị trung bình
 * +step3: giá trị trung bình bằng tổng giá trị 5 số chia 5
 *
 * Output: 3
 */

var so_thuc1 = 1;
var so_thuc2 = 2;
var so_thuc3 = 3;
var so_thuc4 = 4;
var so_thuc5 = 5;

var gia_tri_trung_binh = null;

gia_tri_trung_binh = (so_thuc1 + so_thuc2 + so_thuc3 + so_thuc4 + so_thuc5) / 5;
console.log("Gia tri trung binh:", gia_tri_trung_binh);

/**
 * Bai 3
 * Input: Số USD = 4 , 1 USD = 23.500 VND
 *
 * Step:
 * +step1: Tạo biến nhập số usd và một biến có giá trị 23.500
 * +step2: Tạo biến kết quả
 * +step 3: kết quả bằng số usd * 23.500
 *
 * Output: 94.000
 */

var so_usd = 4;
var quy_doi = 23500;
var so_tien = null;

so_tien = so_usd * quy_doi;
console.log("So tien =", so_tien);

/**
 * Bai 4
 * Input: Chiều dài và chiều rộng hình chữ nhật lần lượt là 4,5
 *
 * Step:
 * +step1: Tạo biến nhập vào chiều dài và biến nhập vào chiều rộng hình chữ nhật
 * +step2: Tạo biến chu vi và biến diện tích
 * +step3: Áp dụng công thức để tình chu vi và diện tích từ chiều dài, chiều rộng hình chữ nhật
 *
 * Output: diện tích = 20, chu vi = 18
 */

var chieu_dai = 5;
var chieu_rong = 4;
var chu_vi;
var dien_tich;

chu_vi = (chieu_dai + chieu_rong) * 2;
console.log("Chu vi:", chu_vi);
dien_tich = chieu_dai * chieu_rong;
console.log("Dien tich:", dien_tich);

/**
 * Bai 5
 *
 * Input: Nhập vào số gồm 2 chữ số 45
 *
 * Step:
 * +step1: Tạo biến nhập vào số gồm 2 chữ số
 * +step2: Tạo biến hàng đơn vị và tính bằng cách lấy số vừa nhập chia lấy dư cho 10
 * +step3: Tạo biến hàng chục và tính bằng cách lấy số của step1 chia cho 10 làm tròn
 * +step4: Tạo biến tổng và tính bằng cách lấy số hàng đơn vị cộng số hàng chục
 *
 * Output: 9
 */

var nhap_so = 45;
var so_hang_don_vi;
var so_hang_chuc;

so_hang_don_vi = nhap_so % 10;

so_hang_chuc = Math.floor(nhap_so / 10);

var tong;
tong = so_hang_don_vi + so_hang_chuc;
console.log("Tong =", tong);
